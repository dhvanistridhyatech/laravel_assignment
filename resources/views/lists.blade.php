@extends('admin.layout.master')
@section('title','Dashboard')
@section('content')


      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> User Info</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                     <th>
                        Id
                      </th>
                      <th>
                        First Name
                      </th>
                       <th>
                        Last Name
                      </th>
                       <th>
                        Gender
                      </th>
                       <th>
                        Email
                      </th>
                      <th>
                        Status
                      </th>
                       <th>
                        Image
                      </th>
                      <th>
                        Password
                      </th>
                       <th>
                        UPDATE
                      </th>
                       <th>
                       DELETE
                      </th>
                    </thead>
                    <tbody>
                     @foreach($data as $value) 
                      <tr>
                        <td>
                         {{ $value->id }}
                        </td>
                        <td>
                          {{ $value->first_name }}
                        </td>
                        <td>
                          {{ $value->last_name }}
                        </td>
                         <td>
                          {{ $value->gender }}
                        </td>
                        <td>
                          {{ $value->email }}
                        </td>
                        <td>
                          {{ $value->status }}
                        </td>
                         <td>
                          {{ $value->profileImage }}
                        </td>
                        <td>
                          {{ $value->password }}
                        </td>
                        <td>
                       <a href="/updateUser/{{ $value->id }}">  <button type="button" class="btn btn-info">UPDATE</button></a>

                        </td>
                        <td>
                       <a href="/deleteUser/{{ $value->id}}">   <button type="button" class="btn btn-warning">DELETE</button></a>
                        </td>

                      </tr>
                      @endforeach

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
@endsection