@extends('admin.layout.master')
@section('section')

  <center>
      <h2>Registration Form </h2>

    {{ Form::open(array('url' => url('/addUserData'), 'class'=>'reg-form')) }}
    {{ csrf_field() }}
    <div class="form-group">
     <label>Enter FirstName</label>
     <input type="text" name="firstName" class="form-control"  value="{{$users->name }}" />
    </div>
     <div class="form-group">
     <label>Enter LastName</label>
     <input type="text" name="lastName" class="form-control" />
    </div>
     <div class="form-group">
      <input type="radio" name="gender" value="female"> Female<br>
     
    </div>
     <div class="form-group">
     <label>Enter EmailAddress</label>
     <input type="email" name="email" class="form-control" />
    </div>
    <div class="form-group">
     <label>Enter Password</label>
     <input type="password" name="password" class="form-control" />
    </div>
    <div class="form-group">
      <input type="file" name="profileimage" id="fileToUpload">
    <input type="submit" value="Upload Image" name="submit">
  </div>
    <div class="form-group">
     <input type="submit" name="submit" class="btn btn-primary" value="submit" />
    </div>
       {!! Form::close() !!}

   </center>

@endsection