@extends('admin.layout.master')
@section('section')
<link href="../../assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="../../assets/css/now-ui-dashboard.css?v=1.5.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../../assets/demo/demo.css" rel="stylesheet" />
<center>
      {{ Form::open(array('url' => url('/editUser'), 'class'=>'reg-form','files'=>true)) }}

      <table width="50%">
        <tbody><h2><strong>Edit User</strong></h2>
          <tr>
            <td class="py-4">{!! Form::label('Id', '' , array('class'=>'form-lable')) !!}</td>
            <td class="py-4">{!! Form::text('id', $data->id, array('class' => 'form-control bg-white')) !!}</td>
          </tr>
          <tr>
            <td class="py-4">{!! Form::label('Fist Name', '' , array('class'=>'form-lable')) !!}</td>
            <td class="py-4">{!! Form::text('firstName', $data->first_name, array('class' => 'form-control bg-white')) !!}</td>
          </tr>
          <tr>
            <td class="py-4">{!! Form::label('Last Name', '' , array('class'=>'form-lable')) !!}</td>
            <td class="py-4">{!! Form::text('lastName',$data->last_name , array('class' => 'form-control bg-white')) !!}</td>
          </tr>

          <tr>
           <td class="py-4">{!! Form::label('Select gender','', array('class'=>'form-lable')) !!}</td>
            <td class="py-4">{!! Form::radio('gender', 'male') !!} male &nbsp;  {!! Form::radio('gender', 'female') !!}  female</td>
          </tr>
          <tr>
            <td class="py-4">{!! Form::label('Email', '' , array('class'=>'form-lable')) !!}</td>
            <td  class="py-4">{!! Form::email('email', $data->email,array('class' => 'form-control bg-white')) !!}</td>
          </tr>

           <tr>
            <td>{!! Form::label('Status', '' , array('class'=>'form-lable')) !!}</td>
            <td>{!! Form::select('status', array('Active' => 'Active', 'Deactive' => 'Deactive')) !!}</td>
          </tr>
          <tr>
            <td class="py-4">{!! Form::label('Profile Image', '' , array('class'=>'form-lable')) !!}</td>
            <td class="py-4">{!! Form::file('profileImage') !!}</td>
          </tr>

          <tr>
            <td class="py-4">{!! Form::label('Password', '' , array('class'=>'form-lable')) !!}</td>
            <td class="py-4">{!! Form::password('password'); !!}</td>
          </tr>
          <tr><td>{!! Form::submit('UPDATE',array('class'=>'btn btn-primary')) !!}</td></tr>

        </tbody>
      </table>
     </center>      

      {!! Form::close() !!}
@endsection