@extends('admin.layout.master')
@section('section')

      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> <center>All Users</center></h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table" border="1px solid black">
                    <thead class=" text-primary">
                     <th bgcolor="#28a74569" style="color: black;border: 1px solid black">
                        Id
                      </th>
                      <th bgcolor="#28a74569" style="color: black;border: 1px solid black">
                        First Name
                      </th>
                       <th bgcolor="#28a74569" style="color: black;border: 1px solid black">
                        Last Name
                      </th>
                       <th bgcolor="#28a74569" style="color: black;border: 1px solid black">
                        Gender
                      </th>
                       <th bgcolor="#28a74569" style="color: black;border: 1px solid black">
                        Email
                      </th>
                       <th bgcolor="#28a74569" style="color: black;border: 1px solid black">
                        Image
                      </th>
                      <th bgcolor="#28a74569" style="color: black;border: 1px solid black">
                        Password
                      </th>
                       <th bgcolor="#28a74569" style="color: black;border: 1px solid black">
                        UPDATE
                      </th>
                       <th bgcolor="#28a74569" style="color: black;border: 1px solid black">
                       DELETE
                      </th>
                    </thead>
                    <tbody style="background-color: #ffb6c178;">

                     @foreach($data as $value) 
                      <tr>
                        <td>
                         {{ $value->id }}
                        </td>
                        <td>
                          {{ $value->first_name }}
                        </td>
                        <td>
                          {{ $value->last_name }}
                        </td>
                         <td>
                          {{ $value->gender }}
                        </td>
                        <td>
                          {{ $value->email }}
                        </td>
                         <td>
                          {{ $value->$article->image_name }}
                        </td>
                        <td>
                          {{ $value->password }}
                        </td>
                        <td>

                       <a href="/editUser/{{ $value->id}}">  <button type="button" class="btn btn-info">UPDATE</button></a>

                        </td>
                        <td>
                       <a href="/deleteUser/{{ $value->id}}">   <button type="button" class="btn btn-warning">DELETE</button></a>
                        </td>

                      </tr>
                      @endforeach

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

   @endsection