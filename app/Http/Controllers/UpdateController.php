<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;

class UpdateController extends Controller
{
    public function updateUserData(Request $req)
    {


     $record = User::find($req->id);
      return view('editUser',['data'=>$record]);
    }
}