
<?php

public function loginForm()
{
    return view('login');
}

/* @POST
*/
public function login(Request $request){
    $this->validate($request, [
        'email' => 'required|email',
        'password' => 'required',
        ]);
    if (\Auth::attempt([
        'email' => $request->email,
        'password' => $request->password])
    ){
        return redirect('/show');
    }
    return redirect('/login')->with('error', 'Invalid Email address or Password');
}
/* GET
*/
public function logout(Request $request)
{
    if(\Auth::check())
    {
        \Auth::logout();
        $request->session()->invalidate();
    }
    return  redirect('/login');
}
?>