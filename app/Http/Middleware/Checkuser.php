<?php

namespace App\Http\Middleware;

use Closure;

class Checkuser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = Auth::User()->role;
        $status = Auth::User()->status;

        if ($role == "User" ||  $status="Active")
        {
             return $next($request);       
        }
        else
        {
            return redirect("/");
        }
    }
}
